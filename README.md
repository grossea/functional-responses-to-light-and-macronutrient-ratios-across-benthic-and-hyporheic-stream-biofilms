# Contrasting functional responses of benthic and hyporheic stream biofilms to light availability and macronutrient stoichiometry

## General Information

This repository belongs to the article "Functional Responses to Light and Macronutrient Ratios Across Benthic and Hyporheic Stream Biofilms" (ADD INFORMATION AND DOI AFTER THE PAPER IS PUBLISHED).

## Data Files

There are tree categorsies of data files: raw data, R code and results tables.

### Raw Data Files

- The [Data.csv](Data.csv) table is the collection of all measured variables. The table is structured in flume ID (1-24), sampling depth (B = benthic, H = hyporheic), sampling day (day 7 or day 14), the value, the unit and the parameter that belong to the value. The parameters include some abbreviations: BD is bacterial density, BP is bacterial production, EPS is extracellular polymeric substance as glucose equivalent, Chl a is chlorophyll a, Functional H' is the Shannon diversity index as functional diversity of the substrates in the Biolog Ecoplates, NUSE and PUSE are the share of N and P containing substrates.

- The [Biolog.csv](Biolog.csv) table contains the absorbance data of the Biolog Ecoplates. The table is structured in sampling depth (B = benthic, H = hyporheic), sampling day (day 7 or day 14), light availability (L- = low, L+ = high), C:N:P ratio (CP- = ambient, CP+ = altered), flume ID (1-24), incubation time of Ecoplates (~every 24h for 7 days after the sampling) as well as the absorbance of the 31 substrates. Absorbances are already corrected  for background absorbance of the well without substrate.

- The [Treatment. csv](Treatments.csv) table contains the treatment information for every flume. The table is structured in flume ID (1-24), light availability (L- = low, L+ = high), C:N:P ratio (CP- = ambient, CP+ = altered) as well as the treatment in general, what is the combination of light availability and C:N:P ratio. 

- The [LoggerMOBICOS.csv](LoggerMOBICOS.csv) table contains the environmental data from the monitoring station located at the experiments site. These data are values for chlorophyll a, conductivity, nitrate, cxygen concentrations and saturation, PAR, turbidity, water temperature and pH value.

- The [LightInput.csv](LightInput.csv) table contains light-data in PAR for 15-min-intervals in September 2020 and 2021. Further descriptions can be found in [comparison_light_intensities.qmd](comparison_light_intensities.qmd).

### R Code

- The key code document is the [DataAnalysis.Rmd](DataAnalysis.Rmd). It contains the entire data analysis of the article. This code document reads the four raw data files ([Data.csv](Data.csv), [Biolog.csv](Biolog.csv), [Treatment. csv](Treatments.csv), [LoggerMOBICOS.csv](LoggerMOBICOS.csv)) and the functions from [Ecoplatesfunctions.R](Ecoplatesfunctions.R). The output of this code are all figures, including the ones from the supporting information of the article. Furthermore the PERMANOVA output tables ([PERMANOVA_date.csv](PERMANOVA_date.csv) and [PERMANOVA_Treatment.csv](PERMANOVA_Treatment.csv)) will be created to summarize the PERMANOVA results investigating effects of sampling day or light, C:N:P ratios and their interaction on the measured variables. The table [LoggerMOBICOS.csv](LoggerMOBICOS.csv) is summarised as mean and standard deviation per parameter over the entire experiments time in the output table [EnvironmentalData.csv](EnvironmentalData.csv) .

- We used a method from [N. Perujo, A.M. Romaní, J.A. Martín-Fernández, Microbial community-level physiological profiles: Considering whole data set and integrating dynamics of colour development](https://doi.org/10.1016/j.ecolind.2020.106628) to analyse the Biolog Ecoplates results in canonical variates plots. The data analysis is part of the [DataAnalysis.Rmd](DataAnalysis.Rmd) code and the adapted functions that are needed are stored in the script [Ecoplatesfunctions.R](Ecoplatesfunctions.R). 

- To further explain the selected light intensities for the low and high light treatment, we added the document [comparison_light_intensities.qmd](comparison_light_intensities.qmd) to classify the selected light intensities in the context of the experiments site. Therefore the [LightInput.csv](LightInput.csv) data from a Master thesis by Frederik Lemke (title: Nitrate assimilation in a stoichiometric and light gradient in a stream under varying land use) were used to get information about the 5th and 95th percentile of the light availabiliy in September at the experiments site. The output of this document is [comparison_light_intensities.html](comparison_light_intensities.html).

### Output Files

- The output table [PERMANOVA_date.csv](PERMANOVA_date.csv) lists the PERMANOVA results testing an effect of the sampling day for every paramter and sampling depth combination

- The output table [PERMANOVA_Treatment.csv](PERMANOVA_Treatment.csv) lists the PERMANOVA results testing an effect of light availability, C:N:P ratio and their interaction for every combination of parameter, sampling depth and sampling day

- The output table [EnvironmentalData.csv](EnvironmentalData.csv) contains the mean values and standard deviations for chlorophyll a, conductivity, nitrate, oxygen concentration and saturation, PAR, turbidity, water temperature and ph-value in the river Holtemme at the experiments site.

- There will be an html output document when knitting the code document [DataAnalysis.Rmd](DataAnalysis.Rmd) containing all important information, comments, output tables and figures that can be downloaded as [DataAnalysis.html](DataAnalysis.html).

- There will be an html output document when knitting the code document [comparison_light_intensities.qmd](comparison_light_intensities.qmd) containing all important information, comments, output tables and figures that can be downloaded as [comparison_light_intensities.html](comparison_light_intensities.html).

## R Setup

The R code in this repository was developed with R version 4.4. and uses the following R libraries:

- [dplyr](https://cloud.r-project.org/web/packages/dplyr/index.html)
- [ggplot2](https://cloud.r-project.org/web/packages/ggplot2/index.html)
- [RColorBrewer](https://cran.r-project.org/web/packages/RColorBrewer/index.html)
- [vegan](https://cran.r-project.org/web/packages/vegan/index.html)
- [zCompositions](https://cran.r-project.org/web/packages/zCompositions/index.html)
- [candisc](https://cran.r-project.org/web/packages/candisc/index.html)
- [readxl](https://cran.r-project.org/web/packages/readxl/index.html)
- [psych](https://cran.r-project.org/web/packages/psych/index.html)
- [magrittr](https://cran.r-project.org/web/packages/magrittr/index.html)
- [car](https://cran.r-project.org/web/packages/car/index.html)
- [pairwiseAdonis](https://github.com/pmartinezarbizu/pairwiseAdonis)
- [multcompView](https://cran.r-project.org/web/packages/multcompView/index.html)
- [ggh4x](https://cran.rstudio.com/web/packages/ggh4x/index.html)

## Disclaimers

The authors do not take any responsibility for damage done to the user system by executing the code of this repository.

The code is distributed under the [BSD-3 license](LICENSE).

The data is distributed under the license given by the journal and/or publisher of the journal the data is published in.

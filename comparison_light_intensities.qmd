---
title: Comparison of daily sum of light irradiation from Steinerne Renne with experimental daily light sum (ambient light treatment)
date: today

format: 
    html:
        toc: true
        embed-resources: true
        code-fold: show
execute: 
  warning: false
---

# Source of ambient stream light conditions

The source is the Stream Light model developed within the Master thesis of Frederik Lemke at UFZ. This stream light model calculates stretch-specific photon flux density (PPFD) at the water surface.

For the modelling of the site-specific PPFD, the StreamLight R Package according to (Savoy et al. 2021) was used. This model estimates the average PPFD at the water surface along a cross-section of the river in µmol m-2 s-1.

StreamLight combines the SHADE2 model according to (Li et al. 2012) for the assessment of shading conditions and the radiative transfer model according to (Campbell und Norman 1998). For this combination, the package requires a site-specific model driver and additional site parameters:

1. According to (Savoy et al. 2021) this function uses hourly information on the directional shortwave radiation [W/m2] from the North American Land Data Assimilation System (NLDAS) and the Leaf Area Index (LAI) from the Moderate Resolution Imaging Spectroradiometer (MODIS). The LAI is a dimensionless measure of the total unilateral area of leaf tissue per m² of ground area used to characterize the canopy of an ecosystem (Bréda 2003). For Germany NLDAS is not available. Instead data from the DWD Climate Data Center (CDC)  on historical 10-minute station measurements of diffuse solar radiation [J/cm2] from the weather station in Wernigerode, Germany (coordinates) were used. 

2. The LAI for each site was derived from the MODIS 4-d 500 m product MCD15A3Hv006. (Myneni et al. 2015) via the Application for Extracting and Exploring Analysis Ready Samples (AppEEARS) after registration at the NASA EOSDIS Land Processes Distributed Active Archive Center, United States, and integrated into the model driver generation function.  

3. Further information used: 
   - The coordinate of the MOBICOS site.
   - Channel width W [m], channel azimuth [degrees from north], bank height [m], bank slope [dimensionless], water level [m], average tree height [m], canopy overhang [m], height of the maximum canopy overhang [m].
   - The parameter of the bank slope was standardized according to the template of  (Savoy et al. 2021) to 100. Bank height and water level were assigned the same value.
   - Bank height, water level and channel width was taken from Weitere et al. 2021
   - Channel azimuth was calculated based on the digital terrain model of the (GeoBasis-DE / LVermGeo LSA 2022a) and the R-package "nvctr" according to (Gade 2010) with the function "altitude_azimuth_distance" along the flow direction in a buffer of 2000 m around the site coordinates. The necessary shapefile of the course of the Holtemme river along the sites was taken from the geodatabase HydroRIVERS v1.0 (Lehner und Grill 2013). 
   - For average tree height, this shapefile of the river course and the digital terrain model were combined with the digital surface model of the (GeoBasis-DE / LVermGeo LSA 2022b) to determine the tree height using the Canopy Height Model according to (Lisein et al. 2013; Wasser et al. 2021)
   - In a buffer of 2000 m around the site coordinates and a buffer of 20 m left and right along the stream shapefiles, all determined heights were filtered from the CHM in the interval of 2 m to 40 m (which should roughly correspond to realistic tree heights that cast shadows) and then their median was taken as the average tree height along the stream at each site. The canopy overhang was taken as 10% of the average tree height and the height of the maximum canopy overhang was taken as 75% of the average tree height (Savoy et al.2021).

# Load libraries and data

```{r libraries}
library(tidyverse)
library(lubridate)
```

```{r load data}
df <- read_csv2("LightInput.csv")[,-1]

str(df)
```

# Prepare data

## Daily ambient (stream surface) light sum calculation 

### Steps

1.  Create date column from date string
2.  Group by date column
3.  Calculate daily light sum based on 15 min time interval

### Assumptions

1.  Stream Light model represents ambient light per second well, modelled ambient light is light at stream surface, hence comparable to experiment measurements above sediment
2.  900 seconds = 15 mins

### Calculation

For each day in the dataset:

$$
Daily~light~sum_{stream} = \sum{Light~per~second_{15min~interval} * 900}
$$

```{r data prep}

df$date <- date(df$solar.time)

light_day <- df %>% 
           group_by(date) %>%
           summarise(light_day_sum = sum(light*15*60))
     
```

### Selection of ambient light data and conversion to mol (µmol gives very high numbers as daily light sum)

1.  Select only data from September 2020 and September 2021
2.  Convert light sum from µmol to mol photons d^-1^ m^-2^

```{r only September}

min(df$date)
max(df$date)

light_day_september <- light_day %>%
  filter(date >= "2020-09-01" & date <= "2020-09-30" | date >= "2021-09-01" & date <= "2021-09-30")

l_day_sep_mol <- light_day_september  %>% 
 mutate(light_sum_mol = light_day_september$light_day_sum / 1000 / 1000)
```

## Histogram of ambient (stream) daily light sum (mol photons d^-1^ m^-2^)

```{r histogram ambient light sum}
hist(l_day_sep_mol$light_sum_mol)
```

### Calculation of daily median and quantiles for ambient light sum

1.  Calculation of median, 5^th^ and 95^th^ quartile (histogram indicative of skewed distribution)

```{r median and quantiles, daily ambient light sum}
median_light_sum <- round(median(l_day_sep_mol$light_sum_mol),2)
quantile_light_sum <- round(quantile(l_day_sep_mol$light_sum_mol, 
                      probs = c(0.05, 0.95)), 2)
```

## Daily light sum of experiment

### Steps:

1.  Calculation of daily light sum for stream light data
2.  Conversion of light to mol photons d^-1^ m^-2^

### Assumptions:

-   light sum of experiment, high light treatments: 90  mol photons s^-1^m^-2^, low light treatments: 20 mol photons s^-1^m^-2^

-   10 hours of light per day in experiment

-   36000 seconds per day

### Calculation: 
$$ 
Daily~light~sum_{experiment} = Light~per~second * seconds / day
$$

```{r daily light sum experiment}

experiment_light_sum <- 90*60*60*10 # (umol / s * m2) * sec * min * 10 h (light phase)

experiment_light_sum_mol <- round(experiment_light_sum / 1000 / 1000,2)

experiment_light_sum_low <- 20*60*60*10 # (umol / s * m2) * sec * min * 10 h (light phase)

experiment_light_sum_mol_low <- round(experiment_light_sum_low / 1000 / 1000,2)

```


# Comparison of stream daily light sum and experiment daily light sum

The experiment daily light sum is `r experiment_light_sum_mol`  mol photons d^-1^ m^-2^ in the high light treatments and `r experiment_light_sum_mol_low`  mol photons d^-1^ m^-2^ in the low light treatments. The median ambient light sum is `r median_light_sum` mol photons d^-1^ m^-2^. The 5^th^ quantile of the daily ambient stream light sum is `r quantile_light_sum[1]` mol photons d^-1^ m^-2^ and the 95^th^ quantile is `r quantile_light_sum[2]` mol photons d^-1^ m^-2^.

In conclusion, the experimental low light treatment is in line with the ambient stream light while the high light treatment is much higher than the ambient light availability but still in a realistic range.